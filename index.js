// Load the expressjs module into our application and save it in a variable called express.
const express = require('express')

// localhost port number
const port = 4000

// app is our server
// create an application that uses and stores it as app
const app = express()

// middleware
// express.json() is a method which allow us to handle the streaming of data and automatically parse the incoming json from our request
app.use(express.json())

// mockdatabase
let users = [
		{
			username: "TStark3000",
			email: "starkindustries@gmail.com",
			password: "notPeterParker"
		},
		{
			username: "ThorThunder",
			email: "loveandthunder@gmail.com",
			password: "iloveStormBreaker"
		}
	]
	
	// express has methods to use as routes corresponding to HTTP methods
	//Syntax:
		//app.method(<endpoint>, function for request and response)
	
	// [HTTP method GET]
		app.get("/", (request, response) => {
			// response.status = writeHead
			// response.send = write with end
			response.status(201).send("Hello from express!")
		})

	// mini-activity
		// create a "get" route in express.js which will be able to send a message in the client
		// endpoint: /greeting
		// meassage: Hello form batch245-surname
		// status code: 201
		app.get("/greeting", (request, response) => {
			// response.status = writeHead
			// response.send = write with end
			response.status(201).send("Hello from batch245-mirafuentes")
		})

		app.get("/users", (request, response) =>{
			response.send(users)
		})

	// [HTTP method POST]
		app.post("/users", (request, response) => {
			let input = request.body

			let newUser = {
				username: input.username,
				email: input.email,
				password: input.password
			}

			users.push(newUser)

			response.send(`The ${newUser.username} is now registered in our website with emal ${newUser.email}`)
		})

	// [HTTP method DELETE]
		app.delete("/users", (request, response) =>{
			let deleted = users.pop()
			response.send(deleted)
		})

	// [HTTP method PUT] - change specific property
		app.put("/users/:index", (request, response) =>{
			let indexToBeUpdated = request.params.index
			console.log(typeof indexToBeUpdated)

			indexToBeUpdated= parseInt(indexToBeUpdated)
			
			if(indexToBeUpdated<users.length){
				users[indexToBeUpdated].password = request.body.password

				response.send(users[indexToBeUpdated])
			}else{
				response.status(404).send("page not found. 404!")
			}		
		})


app.listen(port, () => console.log("Server is running at ", port))

		